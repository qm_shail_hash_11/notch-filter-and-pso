% This is the function that we are trying to minimise
function error = Error(X, params) % Returns error and takes X and params as the arguments
    X = X';
    Q = params.Q;
    P = params.P;
    error = X'*Q*X + P'*X;
end