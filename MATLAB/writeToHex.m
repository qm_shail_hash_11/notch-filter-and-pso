function writeToHex(file_name, data)
    file = fopen(file_name,'w');
    for k=1:length(data)
        fprintf(file,'0x%s, ', hex(data(k)));
    end
    fclose(file);
end