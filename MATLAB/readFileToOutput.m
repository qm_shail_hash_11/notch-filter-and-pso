outputFromBoard = BinToFi('out2.bin');
outputFromBoard = outputFromBoard';
figure;
periodogram(outputFromBoard), title('From Board Output Signal');

function outputFi = BinToFi(file_name)
    f = fopen(file_name, 'r');
    var = fread(f);
    fclose(f);
    len = length(var)/4;
    outputFi = zeros(1,len);
    max32s = 2^31-1;
    max32 = 2^32;
    for i = 1:len
        j = (i-1)*4+1;
        outputFi(i) = var(j) + var(j+1)*(16^2) + var(j+2)*(16^4) + var(j+3)*(16^6);
        if(outputFi(i) > max32s)
            outputFi(i) = outputFi(i) - max32;
        end
        outputFi(i) = outputFi(i)/(2^16);
    end
end