clc;clear;close all;

%% Parameters of the filter
Fs = 256;           %Sampling Frequency
alpha_hz = 4;       %Notch Width
orderM = 32;        %The order of the filter
F0 = 50;            %Notch Frequency
epelon = 0;         %Error w.r.t ideal response
weight = 5;         %Weight variable

%% Calculate cost parameters(Q, P)
alpha = (alpha_hz*pi*2)/Fs;  %alpha in rad
W0=(F0*pi*2)/Fs;             %W0 in rad

N = orderM/2;

Q = zeros(N+1);
for i = 1:N+1
    for j=1:N+1
        uut = @(w) (cos((i-1)*w).*cos((j-1)*w));
        Q(i,j) = integral(uut,0,pi);
        Q(i,j) = Q(i,j)+(weight-1)*integral(uut,W0-alpha/2,W0+alpha/2);
    end
end

P=zeros(N+1,1);
for i= 1:N+1
    u = @(w) cos((i-1)*w);
    P(i) = integral(u,0,pi)+integral(u,W0-alpha/2,W0+alpha/2)*(1-epelon*weight);
end

costParams.Q = Q;
costParams.P = P;

%% solving for X usign PSO
problem.CostFunction = @(X) Error(X, costParams);
problem.nVar = N+1;
problem.VarMin = -2;
problem.VarMax =  2;

% Constriction Coefficients
kappa = 1;
phi1 = 2.05;
phi2 = 2.05;
phi = phi1 + phi2;
chi = 2*kappa/abs(2-phi-sqrt(phi^2-4*phi));

params.MaxIt = 70;
params.nPop = 50;
params.w = chi;
params.wdamp = 0.99;
params.c1 = chi*phi1;
params.c2 = chi*phi2;
params.ShowIterInfo = false;

out = PSO(problem, params);

BestSolution = out.BestSol;
BestCosts = out.BestCosts;

% plot PSO Results
figure;
plot(BestCosts, 'LineWidth', 2);
xlabel('Iteration');
ylabel('Best Cost');
grid on;

X = BestSolution.Position;
X = X';

%% Converting into impulse reponse format
H = zeros(orderM+1,1);
H(orderM/2) = X(1);
for i=0:orderM/2
   H(orderM/2-i+1) = X(i+1)/2;
   H(orderM/2+i+1) = H(orderM/2-i+1);
end

%% Display H
figure;
freqz(H);title('H');
hold on;

%% Converting into fixed point impulse reponse format
Hf = fi(H, 'OverflowAction', 'Saturate', 'Signed',1, 'WordLength', 32, 'FractionLength', 16);
Hd = double(Hf);
freqz(Hd);title('Hf');

%% Convolution in MATLAB
load('eegdata.mat');
%%process data
X = data(1,1);
X = X{1,1}(4);
X = X{1,1}(1,:);
X = X';

y = conv(X,H);

Xf = fi(X, 'OverflowAction', 'Saturate', 'Signed',1, 'WordLength', 32, 'FractionLength', 16);
yf = conv(Xf,Hf);
yd = double(yf);

%% Write to txt file for further access
writeToHex('EEG_Data_hex.txt', Xf');
writeToHex('Filter_Coeff.txt', Hf);

%% Plot data
figure;
periodogram(X), title('Input Signal');
hold on;
figure;
periodogram(y), title('Convolued Output Signal');
hold on;
% figure;
% periodogram(yd);
